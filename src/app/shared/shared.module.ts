import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { RouterModule } from '@angular/router';
import { ImagenPipe } from '../core/pipes/imagen.pipe';

@NgModule({
  imports: [CommonModule, MaterialModule, RouterModule],
  declarations: [DialogConfirmComponent, ImagenPipe],
  exports: [MaterialModule, RouterModule, ImagenPipe],
})
export class SharedModule {}
