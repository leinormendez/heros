
import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from '../interfaces/hero.interface';


@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(hero?: Hero | null): string {
    if(!hero?.id && !hero?.image){
     return 'assets/no-image.png';
    }else {

      return hero?.image as string;
    }
  }

}
