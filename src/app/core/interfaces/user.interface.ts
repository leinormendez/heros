export interface User {
  id?: string;
  usuario?: string;
  email?: string;
  rol?: string;
}
