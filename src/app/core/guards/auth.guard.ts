import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

export const authGuard: CanActivateFn = (route, state) => {
  const authenticationService = inject(AuthenticationService);
  const snackbarService = inject(SnackbarService);

  const router = inject(Router);

  return authenticationService.verifyAuthentication().pipe(
    tap((isAuthenticated) => {
      console.log('authGuard', isAuthenticated);
      snackbarService.openSnackBar('Error');
      if (!isAuthenticated) {
        router.navigate(['/auth/login']);
      }
    })
  );
};
