import { inject } from '@angular/core';
import { CanLoadFn, Router, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';

export const canLoadGuard: CanLoadFn = (
  route: Route,
  segments: UrlSegment[]
) => {
  const authenticationService = inject(AuthenticationService);
  const router = inject(Router);

  return authenticationService.verifyAuthentication().pipe(
    tap((isAuthenticated) => {
      if (!isAuthenticated) {
        router.navigate(['/auth/login']);
      }
    })
  );
};
