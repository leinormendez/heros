import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from './layout/layout.module';
import { SnackbarService } from '../services/snackbar.service';

@NgModule({
  imports: [CommonModule, LayoutModule],
  declarations: [],
  providers: [SnackbarService],
  exports: [LayoutModule],
})
export class CoreModule {}
