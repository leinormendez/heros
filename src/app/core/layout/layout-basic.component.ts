import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'aps-layout-basic',
  templateUrl: './layout-basic.component.html',
  styleUrls: ['./layout-basic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutBasicComponent {
  userLogged$: Observable<User> = this.authService.userLogin$;
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}
  logout() {
    this.authService.logout();
    this.router.navigate(['./auth']);
  }
}
