import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutBasicComponent } from './layout-basic.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AuthBackService } from 'src/app/services/auth-back.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [LayoutBasicComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    SharedModule,
    HttpClientModule,
  ],
  exports: [LayoutBasicComponent],
  providers: [AuthenticationService, AuthBackService],
})
export class LayoutModule {}
