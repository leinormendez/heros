import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, concat } from 'rxjs';
import { concatMap, takeUntil } from 'rxjs/operators';
import { Hero } from '../core/interfaces/hero.interface';
import { HeroesBackService } from './heroes-back.service';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root',
})
export class HeroesServiceService {
  destroy: Subject<void> = new Subject();
  destroy$ = this.destroy.asObservable();

  showSpinner: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  showSpinner$ = this.showSpinner.asObservable();

  heroes: BehaviorSubject<Hero[]> = new BehaviorSubject<Hero[]>([]);
  heroes$ = this.heroes.asObservable();

  heroesSearching: BehaviorSubject<Hero[]> = new BehaviorSubject<Hero[]>([]);
  heroesSearching$ = this.heroesSearching.asObservable();

  hero: BehaviorSubject<Hero> = new BehaviorSubject<Hero>({});
  hero$ = this.hero.asObservable();

  constructor(public heroesDataService: HeroesBackService, private snackbarService: SnackbarService) {}
  onDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  getHeroes() {
    this.showSpinner.next(true);
    this.heroesDataService
      .getHeroes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((hero) => {
        this.heroes.next(hero.reverse());
        this.showSpinner.next(false);
        this.snackbarService.openSnackBar('Successfully');
      });
  }
  getHeroeById(id: string) {
    return this.heroesDataService.getHeroById(id);
  }
  deleteHero(id: string) {
    return this.heroesDataService
      .deleteHero(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((any) => this.getHeroes());
  }
  addHero(hero: Hero): void {
    this.heroesDataService
      .addHero(hero)
      .pipe(takeUntil(this.destroy$))
      .subscribe((any) => {
        this.getHeroes();
      });
  }
  updateHero(hero: Hero) {
    this.heroesDataService.updateHero(hero).subscribe((hero) => {
      this.getHeroes();
    });
  }

  getSerchingHeros(stringToSearch: string) {
    this.showSpinner.next(true);
    if (stringToSearch != '') {
      this.heroesDataService
        .getSerchingHeros(stringToSearch)
        .pipe(takeUntil(this.destroy$))
        .subscribe((heros) => {
          this.heroes.next(heros);
          this.showSpinner.next(false);
        });
    } else {
      this.getHeroes();
      this.showSpinner.next(false);
    }
  }
}
