import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { tap } from 'rxjs/operators';
import { User } from '../core/interfaces/user.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthBackService {
  private urlBase: string = environment.apiEndpoint;
  constructor(private http: HttpClient) {}

  searchForId(id: string): Observable<User> {
    return this.http.get<User>(`${this.urlBase}users/${id}`);
  }
  login(email: string): Observable<User> {
    return this.http.get<User>(`${this.urlBase}users?email=${email}`);
  }
  register(user: User): Observable<User> {
    return this.http.post<User>(`${this.urlBase}users`, user);
  }
}
