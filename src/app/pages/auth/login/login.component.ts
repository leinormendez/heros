import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { User } from 'src/app/core/interfaces/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(public authService: AuthenticationService) {}

  user: User = {
    email: '',
    rol: '',
    usuario: '',
  };

  login() {
    console.log(this.user);
    this.authService.login(this.user.email as string);
  }
}
