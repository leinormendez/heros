import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/core/interfaces/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { catchError, of, throwError } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  user: User = {
    email: '',
    rol: '',
    usuario: '',
  };
  rols = [
    {
      id: 'Admin',
      desc: 'Administrador de la web',
    },
    {
      id: 'User',
      desc: 'Usuario de la web',
    },
  ];
  constructor(
    public authenticationService: AuthenticationService,
    private router: Router,
    private snackbarService: SnackbarService
  ) {}

  register() {
    if (
      this.user.usuario != '' &&
      this.user.email != '' &&
      this.user.rol != ''
    ) {
      this.authenticationService
        .register(this.user)
        .pipe(
          catchError((error) => {

            this.snackbarService.openSnackBar('Error al registrar el usuario');

            return of(error);
          })
        )
        .subscribe((registerUser) => {
          if (registerUser) {
            this.snackbarService.openSnackBar('Se ha registrado el usuario');
          }
          this.router.navigate(['/auth/login']);
        });
    }
  }
}
