import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about.component';
import { AboutRoutingModule } from './about-routing.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AboutComponent],
  imports: [HttpClientModule, AboutRoutingModule, CommonModule],
  exports: [AboutComponent],
  providers: [],
  bootstrap: [],
})
export class AboutModule {}
