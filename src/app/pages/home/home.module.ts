import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';

import { FormsModule } from '@angular/forms';
import { HeroTargetComponentComponent } from 'src/app/components/hero-target/hero-target.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ImagenPipe } from 'src/app/core/pipes/imagen.pipe';
import { AddHeroeComponent } from 'src/app/components/add-heroe/add-heroe.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    FormsModule,
    NgxPaginationModule,
  ],
  declarations: [
    HomeComponent,
    HeroTargetComponentComponent,
    AddHeroeComponent,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
