import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs';
import { AddHeroeComponent } from 'src/app/components/add-heroe/add-heroe.component';
import { Hero } from 'src/app/core/interfaces/hero.interface';
import { User } from 'src/app/core/interfaces/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HeroesServiceService } from 'src/app/services/heroes-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  searchTerm: string = '';
  currentPage: number = 1;
  heros$: Observable<Hero[]> = this.heroesService.heroes$;
  userLogged$: Observable<User> = this.authService.userLogin$;

  private searchTerms$ = new Subject<string>();
  private destroy$ = new Subject<void>();

  constructor(
    private heroesService: HeroesServiceService,
    public matDialog: MatDialog,
    private authService: AuthenticationService
  ) {
    this.searchTerms$.pipe(
      debounceTime(300), // Espera 300ms después de cada pulsación de tecla
      distinctUntilChanged(), // Ignora si el término de búsqueda es el mismo que antes
      takeUntil(this.destroy$)
    ).subscribe( searchedTerms => {
      this.currentPage = 1;
      this.heroesService.getSerchingHeros(searchedTerms.trim())
    });
  }

  ngOnInit() {
    this.heroesService.getHeroes();
  }
  ngOnDestroy(): void {
    this.heroesService.onDestroy();
  }

  search() {
    this.searchTerms$.next(this.searchTerm);
  }

  edit(hero: Hero) {
    const dialogRef = this.matDialog.open(AddHeroeComponent, {
      data: hero,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.heroesService.updateHero(result);
      }
    });
  }

  add() {
    const dialogRef = this.matDialog.open(AddHeroeComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.heroesService.addHero(result);
      }
    });
  }
}
