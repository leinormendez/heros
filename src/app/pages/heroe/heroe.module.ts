import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HeroeRoutingModule } from './heroe-routing.module';
import { CommonModule } from '@angular/common';
import { HeroeComponent } from './heroe.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HeroeComponent],
  imports: [HttpClientModule, HeroeRoutingModule, CommonModule, SharedModule],
  exports: [HeroeComponent],
  providers: [],
  bootstrap: [],
})
export class HeroeModule {}
