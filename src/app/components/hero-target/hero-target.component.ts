import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { DialogConfirmComponent } from 'src/app/shared/components/dialog-confirm/dialog-confirm.component';
import { HeroesServiceService } from '../../services/heroes-service.service';
import { Hero } from 'src/app/core/interfaces/hero.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-hero-target',
  templateUrl: './hero-target.component.html',
  styleUrls: ['./hero-target.component.scss'],
})
export class HeroTargetComponentComponent implements OnInit {
  @Input() hero!: Hero;
  @Input() isAdmin!: boolean;
  @Output() public heroToEmit: EventEmitter<Hero> = new EventEmitter<Hero>();
  constructor(
    private heroesServiceService: HeroesServiceService,
    public matDialog: MatDialog,

  ) {}

  ngOnInit() {

  }
  edit() {
    this.heroToEmit.emit(this.hero);
  }


  deleteHero() {
    const dialogRef = this.matDialog.open(DialogConfirmComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.heroesServiceService.deleteHero(this.hero.id as string);
      }
    });
  }
}
